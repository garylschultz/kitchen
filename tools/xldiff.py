''' xldiff.py
A very simplistic diff tool for Excel files that look like pandas DataFrames.

Put the following in your .zshrc file for ease of use:

    xldiff () {
        python ~/Code/kitchen/tools/xldiff.py $* | less
    }

Then run it like this:

    xldiff a.xlsx b.xlsx
'''

import getopt
import numpy as np
import pandas as pd  # type: ignore
import sys

PROMPT = '\u274C'  # red X
SEP = '\u2753'  # red ?
TRUNCATE = 6  # default # of characters from each of two versions per cell


class UsageError(Exception):
    pass

def get_sheetnames(file0: str, file1: str) -> list[list[str]]:
    ''' Produce list of len 2 of list of sheets.
        Print the checks on sheet availability to stdout.
    '''
    sheets = []
    for f in [file0, file1]:
        with pd.ExcelFile(f) as x:
            sheets.append(x.sheet_names)
    return sheets


def pad_cols(df, n: int):
    if n <= 0:
        return df
    m = df.shape[1]
    p = pd.DataFrame('', index=df.index, columns=range(m, m+n))
    return pd.concat([df, p], axis=1)


def pad_rows(df, n: int):
    if n <= 0:
        return df
    m = df.shape[0]
    p = pd.DataFrame('', index=range(m, m+n), columns=df.columns)
    return pd.concat([df, p], axis=0)


def pad(df0, df1):
    ''' Assumes all indices and columns are 0..n-1 for some n. '''
    n = df0.shape[1] - df1.shape[1]
    df0 = pad_cols(df0, -n)
    df1 = pad_cols(df1, n)
    n = df0.shape[0] - df1.shape[0]
    df0 = pad_rows(df0, -n)
    df1 = pad_rows(df1, n)
    return df0, df1


def diffsheet(sheet_name, file0, file1, *, trunc):
    ''' Print the diff summary for the common sheet. '''
    df0 = pd.read_excel(file0, sheet_name,
            header=None, keep_default_na=False, dtype=str).astype(str)
    df1 = pd.read_excel(file1, sheet_name,
            header=None, keep_default_na=False, dtype=str).astype(str)
    df0, df1 = pad(df0, df1)
    dif = ( df0.applymap(lambda s: s[:trunc])
            + SEP
            + df1.applymap(lambda s: s[:trunc])
        )
    dif[df1 == df0] = np.nan
    dif = dif.dropna(axis=0, how='all').dropna(axis=1, how='all')
    counter = dif.isna()
    dif = dif.fillna(value='')
    if not dif.empty:
        rows = counter.shape[0]
        cols = counter.shape[1]
        same = counter.sum().sum()
        missing = rows * cols - same
        sys.stderr.flush()
        print(f'\n{PROMPT} {sheet_name} differs in')
        print(f'\t {rows} rows, {cols} columns, and {missing} cells:')
        print(show_excelish(dif), flush=True)
    return dif


def show_excelish(df):
    ''' Make it look "excel-ish", by replacing 0..n-1 indices with Excel
        A1 type labels: Columns A,B,...,XFD, and rows 1,2,...,1_048_576.
        Sizes: XFD == 16_384 == 2**14 and 1_048_576 == 2**20.
    '''
    df = df.copy()
    df.index = df.index + 1  # 1 based index
    df.columns = pd.Index((xlcol_letters(df.columns + 1)))
    return df


def xlcol_letters(ns):
    ''' A generator to produce the Excel columns A,B,...,XFD, as needed.
        Given an iterable of one-based indices, yield the columns as strings..
        Numbers <= 0 will yield an empty string.
    '''
    for n in ns:
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        yield string


def main(argv: list[str]) -> int:
    '''
    '''
    flags = [a for a in argv if a.startswith("-")]
    files = [a for a in argv if not a.startswith("-")]
    if len(files) != 2:
        raise UsageError(f'need two files, but given {files}')
    try:
        opts, args = getopt.getopt(flags, "", ['trunc='])
    except getopt.GetoptError as err:
        raise err
    trunc = int(TRUNCATE)
    for o, a in opts:
        if o == '--trunc':
            trunc = int(a)
        else:
            assert False, f"unhandled option: {o}={a}"

    sheets = get_sheetnames(*files)

    # Do this with lists, rather than sets, to preserve order.
    both  = [s for s in sheets[0] if s in sheets[1]]
    only0 = [s for s in sheets[0] if s not in sheets[1]]
    only1 = [s for s in sheets[1] if s not in sheets[0]]

    if only0:
        sys.stderr.flush()
        print(f'\n{PROMPT} {files[1]} missing sheets: {only0}', flush=True)
    if only1:
        sys.stderr.flush()
        print(f'\n{PROMPT} {files[0]} missing sheets: {only1}', flush=True)
    for s in both:
        diffsheet(s, *files, trunc=trunc)
    return 0


if __name__ == '__main__':
    exit(main(sys.argv[1:]))
