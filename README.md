# Kitchen

Kitchen sink version of a conda environment.

Also contains a framework for a CLI, with "help" and "env" commands.

### Why?

So often, I want to just try something data-ish and don't want to make
a new environment every time.
This should be a "kitchen sink" that has most of the data science
stuff already there.

## Setting up the environment

The source specification for the environment is in `cenv.rundir/env.yaml`.
The `cenv.rundir/list_env.txt` and `requirements.txt` files are outputs.

To view the environment name and check it corresponds to the one specified
in the yaml document:

```sh
python run env
```

To create or update the environment:

```sh
python run env --update
```

To view help on the environment management:

```sh
python run help env
```

## Using it

The following aliases in your `.bashrc` or equivalent are useful:

```sh
alias cenv='conda activate `head -1 cenv.txt`'
alias run='python run'
```
The first one will activate the right environment,
because the `env` command writes the environment name to
the file `cenv.txt`.
The second one makes it easy to use the commands. For example,

```sh
run env
```
